package com.example.icode_task_b;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final int CREATE_TASK_REQUEST_CODE = 1001; // Вы можете выбрать любое целое число
    private RecyclerView recyclerView;
    private TaskAdapter taskAdapter;

    private TextView hello_user;

    private ImageView add;

    List<Task> taskList = new ArrayList<>();

    String username = ""; // Объявляем переменную здесь

    View.OnClickListener onClick;

    String s = "";

    public static final int PICK_IMAGE = 1; // Код запроса для выбора изображения


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        hello_user = findViewById(R.id.textView3);
        add = findViewById(R.id.imageView5);

        Intent intent = getIntent();
        if (intent.hasExtra("textKey")) {
            username = intent.getStringExtra("textKey"); // Присваиваем значение переменной
            Log.d("USERNAME:", username);
            hello_user.setText("Hi, " + username + "!");
            s = username;
        }

        onClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CreateTaskActivity.class);
                String textToPass = s;

                // Положите текст в интент
                intent.putExtra("textKey", textToPass);
                startActivityForResult(intent, CREATE_TASK_REQUEST_CODE); // Здесь используйте CREATE_TASK_REQUEST_CODE
            }
        };
        add.setOnClickListener(onClick);

        taskAdapter = new TaskAdapter();
        recyclerView.setAdapter(taskAdapter);

        loadTasksFromApi();
    }

    // Вынесите метод openGallery за пределы onCreate
    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, PICK_IMAGE);
    }


    private void loadTasksFromApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://94.198.220.135:3000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService apiService = retrofit.create(ApiService.class);

        Call<List<Task>> call = apiService.getTodayTasks(username);

        call.enqueue(new Callback<List<Task>>() {
            @Override
            public void onResponse(Call<List<Task>> call, Response<List<Task>> response) {
                if (response.isSuccessful()) {
                    List<Task> tasks = response.body();
                    taskAdapter.setTasks(tasks); // Устанавливаем данные в адаптер

                    // Обновляем значение itemCountTextView
                    int itemCount = tasks.size();
                    TextView itemCountTextView = findViewById(R.id.textView2);
                    itemCountTextView.setText("Today you have " + itemCount + " more\n tasks to do!");
                } else {
                    Toast.makeText(MainActivity.this, "Failed to load tasks.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Task>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Network error: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // Метод для обновления адаптера с новой задачей
    public void addTaskToAdapter(Task task) {
        taskAdapter.addTask(task); // Метод addTask() должен быть у вас в адаптере
        taskAdapter.notifyDataSetChanged(); // Обновляем адаптер
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CREATE_TASK_REQUEST_CODE && resultCode == RESULT_OK) {
            // Получаем новую задачу из Intent
            Task newTask = (Task) data.getSerializableExtra("newTask");

            // Добавляем новую задачу в адаптер и обновляем отображение
            addTaskToAdapter(newTask);
        }
    }



}
