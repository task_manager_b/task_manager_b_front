package com.example.icode_task_b;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("get_today_tasks")
    Call<List<Task>> getTodayTasks(@Query("username") String username);
}
