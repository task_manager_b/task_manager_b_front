package com.example.icode_task_b;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogInActivity extends AppCompatActivity {

    TextView error_text;
    Button button;

    EditText username;
    SharedPreferences prefs;
    SharedPreferences.Editor prefs_editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        error_text = findViewById(R.id.error_text);
        button = findViewById(R.id.LogIn);
        username = findViewById(R.id.username);
        prefs=getApplicationContext().getSharedPreferences("button",0);
        prefs_editor=prefs.edit();

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Получаем текст из текстового поля
                String text = username.getText().toString().trim();

                // Проверяем, пустое ли текстовое поле
                if (text.isEmpty()) {
                    // Если поле пустое, выводим ошибку
                    error_text.setVisibility(View.VISIBLE);
                    error_text.setText("Error: The username is incorrect");

                    // Преобразуем HEX-код (#252E5C) в цвет RGB
                    int myColor = Color.parseColor("#252E5C");

                    // Устанавливаем цвет фона кнопки
                    button.setBackgroundColor(myColor);

                } else if (username.length() >= 1 && username.length() < 3) {
                    // Если количество символов меньше 3, выводим ошибку
                    error_text.setText("Error: Min length 3 characters");
                    error_text.setVisibility(View.VISIBLE); // Делаем текст ошибки видимым

                } else if (username.length() > 20) {
                    // Если количество символов меньше 3, выводим ошибку
                    error_text.setText("Error: Max length 20 characters");
                    error_text.setVisibility(View.VISIBLE); // Делаем текст ошибки видимым

                    // Преобразуем HEX-код (#252E5C) в цвет RGB
                    int myColor = Color.parseColor("#252E5C");

                    // Устанавливаем цвет фона кнопки
                    button.setBackgroundColor(myColor);

                } else if (containsRussianLetters(text)) { // Добавляем проверку на русские буквы
                    // Если в тексте есть русские буквы, выводим ошибку
                    error_text.setText("Error: Text contains Russian letters");
                    error_text.setVisibility(View.VISIBLE); // Делаем текст ошибки видимым
                } else {
                    // Если поле не пустое и не содержит русские буквы, скрываем ошибку (если она была видимой)
                    error_text.setVisibility(View.INVISIBLE);

                    // Преобразуем HEX-код (#252E5C) в цвет RGB
                    int myColor = Color.parseColor("#6680FF");

                    // Устанавливаем цвет фона кнопки
                    button.setBackgroundColor(myColor);

                    prefs_editor.putString("username", username.getText().toString());
                    prefs_editor.apply();

                    Intent intent = new Intent(LogInActivity.this, MainActivity.class);

                    String textToPass = username.getText().toString();

                    // Положите текст в интент
                    intent.putExtra("textKey", textToPass);

                    startActivity(intent);
                    finish(); // Завершаем текущую активити
                }
            }
        });
    }

    public boolean containsRussianLetters(String text) {
        // Создаем паттерн для поиска русских букв
        Pattern pattern = Pattern.compile("[а-яА-Я]+");

        // Создаем Matcher для текста
        Matcher matcher = pattern.matcher(text);

        // Проверяем, есть ли русские буквы в тексте
        return matcher.find();
    }
}