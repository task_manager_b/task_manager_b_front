package com.example.icode_task_b;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {


    private static int SPLASH_TIME_OUT = 2000; // Время отображения Splash Screen (2 секунды)

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Этот код будет выполнен после завершения времени отображения Splash Screen
                Intent intent = new Intent(SplashActivity.this, LogInActivity.class);
                startActivity(intent);
                finish(); // Завершаем текущую активити
            }
        }, SPLASH_TIME_OUT);
    }
}