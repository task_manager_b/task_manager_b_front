package com.example.icode_task_b;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class CreateTaskActivity extends AppCompatActivity {
    private EditText titleEditText, dateEditText, descriptionEditText, categoryEditText;

    String baseText;

    Button save;
    HTTPHandler handler;
    View.OnClickListener onClick;
    SharedPreferences prefs;

    SharedPreferences.Editor prefs_editor;

    ImageView home;

    ImageView bin;

    String username = ""; // Объявляем переменную здесь

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_task);

        handler = new HTTPHandler();
        titleEditText = findViewById(R.id.title);
        dateEditText = findViewById(R.id.editText);
        descriptionEditText = findViewById(R.id.editText3);
        categoryEditText = findViewById(R.id.imageView3);
        home = findViewById(R.id.imageView10);
        save = findViewById(R.id.save);
        bin = findViewById(R.id.imageView7);
        prefs = getApplicationContext().getSharedPreferences("auth", 0);

        Intent intent = getIntent();
        if (intent.hasExtra("textKey")) {
            username = intent.getStringExtra("textKey"); // Присваиваем значение переменной
            Log.d("USERNAME - From CTA:", username);
        } else {
            Log.d("USERNAME - From CTA:", "textKey not found in intent");
        }

        onClick = new View.OnClickListener() {
            public void onClick(View v) {
                // Используйте значения из соответствующих EditText, а не prefs.getString
                String title = titleEditText.getText().toString();
                String date = dateEditText.getText().toString();
                String description = descriptionEditText.getText().toString();
                String category = categoryEditText.getText().toString();

                // Создаем новый объект Task
                Task newTask = new Task(title, description); // Используйте конструктор для создания новой задачи
                newTask.setDate(date);
                newTask.setCategory(category);

                // Возвращаем новую задачу в MainActivity
                Intent resultIntent = new Intent();
                resultIntent.putExtra("newTask", newTask);
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        };
        save.setOnClickListener(onClick);

        onClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateTaskActivity.this, MainActivity.class);
                startActivity(intent);
            }
        };
        home.setOnClickListener(onClick);

        onClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CreateTaskActivity.this, MainActivity.class);
                startActivity(intent);
            }
        };
        bin.setOnClickListener(onClick);

    }
}
