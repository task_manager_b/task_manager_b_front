package com.example.icode_task_b;

import java.io.Serializable;

public class Task implements Serializable {

    private String id;
    private String title;
    private String category;

    private String date;
    private String description;

    public Task(String title, String description) {
        this.title = title;
        this.date = date;
        this.description = description;
        this.category = category;
    }

    // Геттеры и сеттеры для каждого поля
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Task{" +
                "title='" + title + '\'' +
                ", date='" + date + '\'' +
                ", description='" + description + '\'' +
                ", category='" + category + '\'' +
                '}';
    }
}